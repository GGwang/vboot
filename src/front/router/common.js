/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : </li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月04日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
import Dashboard from '../views/dashboard';
import PageNotFound from '../views/ErrorPage/PageNotFound';
import Reject from '../views/ErrorPage/Reject';

export default [
  {
    path: 'dashboard',
    name: Dashboard.name,
    component: Dashboard,
    meta: {
      navText: '桌面'
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: resolve => require(['../views/Profile'], resolve),
    meta: {
      navText: '个人资料'
    },
    children: [
      {
        path: 'avatar',
        name: 'ProfileAvatar',
        component: resolve => require(['../views/Profile/Avatar'], resolve),
        meta: {
          navText: '修改头像'
        }
      }
    ]
  },
  {
    path: '404',
    name: PageNotFound.name,
    component: PageNotFound,
    meta: {
      navText: '404'
    }
  },
  {
    path: '/reject',
    name: 'Reject',
    component: Reject
  }
];
