/**
 *<p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : Model窗口的</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月04日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */

export default {
  mounted() {
    $(this.$refs.modal).modal();
    $(this.$refs.modal).on('hidden.bs.modal', () => {
      this.$router.back();
    });
  },
  beforeRouteLeave(to, from, next) {
    if ($('body').hasClass('modal-open')) {
      $(this.$refs.modal).off('hidden.bs.modal');
      $(this.$refs.modal).modal('hide');
    }
    next();
  }
};
