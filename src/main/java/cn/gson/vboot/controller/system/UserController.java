package cn.gson.vboot.controller.system;

import cn.gson.vboot.common.AcLog;
import cn.gson.vboot.common.JsonResult;
import cn.gson.vboot.model.pojo.User;
import cn.gson.vboot.service.RoleService;
import cn.gson.vboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>****************************************************************************</p>
 * <p><b>Copyright © 2010-2018 soho team All Rights Reserved<b></p>
 * <ul style="margin:15px;">
 * <li>Description : 角色管理</li>
 * <li>Version     : 1.0</li>
 * <li>Creation    : 2018年08月08日</li>
 * <li>@author     : ____′↘夏悸</li>
 * </ul>
 * <p>****************************************************************************</p>
 */
@RestController
@RequestMapping("/system/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @GetMapping("/list")
    @AcLog(module = "用户", action = "列表")
    public JsonResult list() {
        return JsonResult.success(userService.list());
    }

    @GetMapping("/get")
    @AcLog(module = "用户", action = "获取", params = "id")
    public JsonResult get(Long id) {
        return JsonResult.success(userService.getById(id));
    }

    @PostMapping("/updateEnable")
    @AcLog(module = "用户", action = "更新状态", params = {"id", "enable"})
    public JsonResult updateEnable(Long id, Boolean enable) {
        userService.updateEnable(id, enable);
        return JsonResult.success();
    }

    @PostMapping("/save")
    @AcLog(module = "用户", action = "创建")
    public JsonResult save(@Valid User user) {
        user.setId(null);
        userService.saveOrUpdate(user);
        return JsonResult.success();
    }

    /**
     * 更新用户
     *
     * @param id   定义出来，表示 更新是 id 一定需要给定值
     * @param user
     * @return
     */
    @PostMapping("/update")
    @AcLog(module = "用户", action = "更新", params = "id")
    public JsonResult update(@RequestParam long id, @Valid User user) {
        userService.saveOrUpdate(user);
        return JsonResult.success();
    }

    @GetMapping("/delete")
    @AcLog(module = "用户", action = "删除", params = "id")
    public JsonResult delete(Long id) {
        userService.deleteById(id);
        return JsonResult.success();
    }

    @GetMapping("/roleList")
    public JsonResult roleList() {
        return JsonResult.success(roleService.getRoleListByEnable(true));
    }

    @GetMapping("/resetPassword")
    @AcLog(module = "用户", action = "重置密码", params = "id")
    public JsonResult resetPassword(Long id) {
        userService.resetPassword(id);
        return JsonResult.success();
    }
}
